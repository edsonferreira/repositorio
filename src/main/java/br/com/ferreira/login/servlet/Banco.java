package br.com.ferreira.login.servlet;

import java.util.ArrayList;
import java.util.List;

public class Banco {

	private static List<Usuario> lista = new ArrayList<Usuario>();
	
	public void adiciona(Usuario usuario) {
		lista.add(usuario);
	}
	
	public List<Usuario> getUsuarios(){
		return Banco.lista;
	}

}
