package br.com.ferreira.login.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ListaUsuarioServlet
 */
@WebServlet("/listaUsuario")
public class ListaUsuarioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ListaUsuarioServlet() {
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Banco banco = new Banco();
		List<Usuario> lista = banco.getUsuarios();
		PrintWriter out = response.getWriter();
		
		out.println("<html><body>");
		out.println("<ul>");
		for (Usuario usuario : lista) {
			out.println("<li>" + usuario.getNome() + "</li>");
		}
		
		out.println("</html></body>");
	}

}
