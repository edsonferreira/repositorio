package br.com.ferreira.login.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Usuario
 */
@WebServlet("/usuario")
public class UsuarioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		System.out.println("Cadastro de Usuário.");
		String nomeUsuario = request.getParameter("nome");

		Usuario usuario = new Usuario();
		usuario.setNome(nomeUsuario);
//		usuario.setEmail(email);
//		usuario.setSenha(senha);
		
		Banco banco = new Banco();
		banco.adiciona(usuario);
		
		PrintWriter out = response.getWriter();
		out.println("<html><body>O Usuário " +nomeUsuario + " foi cadastrado com sucesso!</body></html>");
	}

}
